package pin;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PIN {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Character correctPin[] = {'3', '8', '2', '7', '1'};
        String pinCode;
        int counterStart = 0;
        do {
            pinCode = takePin();
            if (isEqual(changeCharToInt(correctPin), changeStringToChar(pinCode))) {
                break;
            } else if (pinCode.length() != 5) {
                System.out.println("Error, za mało lub dużo znaków");
                counterStart++;
            } else {
                System.out.println("Błędny PIN");
                counterStart++;
            }

            while (counterStart > 2) {
                System.out.println("You LOST");
                break;
            }

        } while (counterStart < 3);

        System.out.println(isEqual(changeCharToInt(correctPin), changeStringToChar(pinCode)));
    }

    private static boolean isEqual(int changeCharToInt, int changeStringToChar) {
        return changeCharToInt == changeStringToChar;

    }

    private static int changeStringToChar(String pinCode) {
        Character pinChar[] = new Character[5];
        for (int indexOfNumber = 0; indexOfNumber < 5; indexOfNumber++) {
            pinChar[indexOfNumber] = pinCode.charAt(indexOfNumber);
        }

        return changeCharToInt(pinChar);
    }

    private static int changeCharToInt(Character[] correctPin) {
        int result = correctPin[correctPin.length - 1] - 48;
        int dec = 1;
        for (int counter = correctPin.length - 2; counter >= 0; counter--) {
            dec = dec * 10;
            result = result + (dec * (correctPin[counter] - 48));
        }
        return result;
    }

    private static String takePin() {
        String pinCode = "";
        System.out.println("PIN");
        try {
            pinCode = scanner.next();
        } catch (InputMismatchException e) {
            System.out.println("What are You doing? Please, give me numbers");
        }
        return pinCode;
    }
}
